using System;
using System.Collections.Generic;
using System.IO;

public class Rover
{
    private static List<Node> nodes = new List<Node>();

    public static void CalculateRoverPath(int[,] map)
    {
        GenerateNodes(map);
        List<Node> listNodes = new List<Node>();
        nodes[0].Distance = 0;
        listNodes.Add(nodes[0]);
        int cost = 0;
        int steps = 0;
        int calculatedDistance = 0;
        while (listNodes.Count > 0)
        {
            Node current = listNodes[0];
            if (current == nodes[nodes.Count - 1])
            {
                List<Node> path = new List<Node>();
                steps++;
                calculatedDistance = current.Distance;
                path.Add(current);
                current = current.LastNode;
                path.Add(current);
                while (current != nodes[0])
                {
                    steps++;
                    current = current.LastNode;
                    path.Add(current);
                }
                SaveResult(path, calculatedDistance, steps);
            }
            listNodes.RemoveAt(0);
            for (int i = 0; i < 4; i++)
            {
                Node neigbor = current.getNeigbor(i);
                if (neigbor == null)
                {
                    continue;
                }
                int distance = current.Distance;
                distance += 1 + current.getCost(i);
                if (neigbor.Distance == int.MaxValue)
                {
                    neigbor.Distance = distance;
                    neigbor.LastNode = current;
                    cost = distance;
                    listNodes.Add(neigbor);
                }
                else if (distance < neigbor.Distance)
                {
                    neigbor.LastNode = current;
                    neigbor.Distance = distance;
                    cost = distance;
                }

                listNodes.Sort((x, y) => x.Distance.CompareTo(y.Distance));
            }
        }
    }

    private static void SaveResult(List<Node> path, int cost, int steps)
    {
        path.Reverse();
        string pathText = "";
        foreach (var item in path)
        {
            pathText += item.ToString() + "->";
        }
        pathText = pathText.Remove(pathText.Length - 2, 2);
        pathText += Environment.NewLine + $"steps: {steps}" + Environment.NewLine + $"fuel: {cost}";
        try
        {
            using (StreamWriter stream = new StreamWriter(Path.Combine(Environment.CurrentDirectory, "path-plan.txt"), false, System.Text.Encoding.Default))
            {
                stream.WriteLine(pathText);
            }
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    private static void GenerateNodes(int[,] map)
    {
        int rows = map.GetUpperBound(0) + 1;
        int columns = map.Length / rows;
        int cost;
        int index = 0;
        for (int i = 0; i < map.Length; i++)
        {
            nodes.Add(new Node());
        }
        int counter = 0;
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                int[] current = new int[] { i, j };
                nodes[counter].SetCoords(current);
                var ways = getPossibleWays(current, rows, columns);
                foreach (var nextNode in getPossibleWays(current, rows, columns, nodes, counter))
                {
                    if (nextNode == null)
                    {
                        index++;
                        continue;
                    }

                    cost = Math.Abs(map[current[0], current[1]] - map[ways[index][0], ways[index][1]]);

                    nodes[counter].AddNeigbor(index, nextNode);
                    nodes[counter].setCost(index, cost);
                    index++;
                }
                index = 0;
                counter++;
            }
        }
    }

    private static Node[] getPossibleWays(int[] current, int rows, int columns, List<Node> nodes, int counter)
    {
        Node[] result = new Node[4];
        if (current[0] + 1 < rows)
        {
            result[2] = nodes[counter + columns];
        }
        if (current[0] - 1 >= 0)
        {
            result[0] = nodes[counter - columns];
        }
        if (current[1] + 1 < columns)
        {
            result[1] = nodes[counter + 1];
        }
        if (current[1] - 1 >= 0)
        {
            result[3] = nodes[counter - 1];
        }
        return result;
    }

    private static int[][] getPossibleWays(int[] current, int rows, int columns)
    {
        int[][] result = new int[4][];
        if (current[0] + 1 < rows)
        {
            result[2] = (new[] { current[0] + 1, current[1] });
        }
        if (current[0] - 1 >= 0)
        {
            result[0] = (new[] { current[0] - 1, current[1] });
        }
        if (current[1] + 1 < columns)
        {
            result[1] = (new[] { current[0], current[1] + 1 });
        }
        if (current[1] - 1 >= 0)
        {
            result[3] = (new[] { current[0], current[1] - 1 });
        }
        return result;
    }

    private class Node
    {
        private int[] coords;
        private int[] cost = new int[4];
        public int Distance { get; set; }
        public Node LastNode { get; internal set; }

        private Node[] neigbors = new Node[4];

        public Node()
        {
            Distance = int.MaxValue;
        }

        public void SetCoords(int[] coords)
        {
            this.coords = coords;
        }

        public override string ToString()
        {
            return $"[{coords[0]}][{coords[1]}]";
        }

        public void AddNeigbor(int index, Node node)
        {
            neigbors[index] = node;
        }

        public Node getNeigbor(int i)
        {
            return neigbors[i];
        }

        public void setCost(int index, int cost)
        {
            this.cost[index] = cost;
        }

        internal int getCost(int index)
        {
            return cost[index];
        }
    }
}